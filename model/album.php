<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class album {

    private $id, $song_count, $title, $release_date, $artist;

    public function __construct($id = -1) {
        if ($id != -1) {
            $this->load($id);
        } else {
            
        }
    }
    
    public function dump_json(){
         return json_encode(get_object_vars($this));
    }

    public function create_album($song_count, $title, $release_date, $artist) {
        $this->set_title($title);
        $this->set_song_count($song_count);
        $this->set_release_date($release_date);
        $this->set_artist($artist);
        return $this->save();
    }

    private function load($id) {
        $albums = json_decode(file_get_contents("../model/albums.json"));
        $this->set_id($id);
        $this->set_title($albums[$id]->title);
        $this->set_song_count($albums[$id]->count);
        $this->set_release_date($albums[$id]->release_date);
        $this->set_artist($albums[$id]->artist);
    }

    private function save() {
        // get the file
        $albums = json_decode(file_get_contents("../model/albums.json"));
        $album_array = array("count" => $this->song_count, "title" => $this->title, "artist" => $this->artist, "release_date" => $this->release_date);
        // if id is there update 
        if ($this->id == "") {
            $albums[] = $album_array;
            $id = end($albums);
        } else {
            $albums[$this->id] = $album_array;
            $id = $this->id;
        }
        // else insert
        file_put_contents("../model/albums.json", json_encode($albums));
        return $id;
    }

    // gets
    public function get_id() {
        return json_encode($this->id);
    }

    public function get_song_count() {
        return json_encode($this->song_count);
    }

    public function get_title() {
        return json_encode($this->title);
    }

    public function get_release_date() {
        return json_encode($this->release_date);
    }

    public function get_artist() {
        return json_encode($this->release_date);
    }

    // sets
    protected function set_id($id) {
        // validation to check the $id attempted
        $this->id = $id;
    }

    protected function set_song_count($count) {
        // validation
        $this->song_count = $count;
    }

    protected function set_title($title) {
        // validation
        $this->title = $title;
    }

    protected function set_release_date($date) {
        // validation
        $this->release_date = $date;
    }

    protected function set_artist($artist) {
        // validation - need to check if the artist exists?
        $this->artist = $artist;
    }

}
