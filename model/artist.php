<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class artist {

    private $id, $name, $dob;

    public function __construct($id = -1) {
        if ($id != -1) {
            $this->load($id);
        } else {
            
        }
    }

    public function dump_json() {
        return json_encode(get_object_vars($this));
    }

    public function create_artist($name, $dob) {
        $this->set_name($name);
        $this->set_dob($dob);
        return $this->save();
    }

    private function load($id) {
        $albums = json_decode(file_get_contents("../model/artists.json"));
        $this->set_id($id);
        $this->set_name($albums[$id]->name);
        $this->set_dob($albums[$id]->dob);
    }

    private function save() {
        // get the file
        $artists = json_decode(file_get_contents("../model/artists.json"));
        $artist_array = array("dob" => $this->dob, "name" => $this->name, "id" => $this->id);
        // if id is there update 
        if ($this->id == "") {
            $artists[] = $artist_array;
            $id = end($artists);
        } else {
            $artists[$this->id] = $artist_array;
            $id = $this->id;
        }
        // else insert
        file_put_contents("../model/artists.json", json_encode($artists));
        return $id;
    }

    // gets
    public function get_dob() {
        return json_encode($this->dob);
    }

    public function get_name() {
        return json_encode($this->name);
    }

    public function get_id() {
        return json_encode($this->id);
    }

    // sets
    protected function set_dob($dob) {
        // validation
        $this->dob = $dob;
    }

    protected function set_name($name) {
        // validation
        $this->name = $name;
    }

    protected function set_id($id) {
        // validation
        $this->id = $id;
    }

}
