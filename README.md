# albums
Here I have made two classes: artists and albums - they do not have inheritance but the albums would inherit from the artists given more time.<br />
It is effectively just giving the ability to create, save and load artists and albums with no frills <br />
In the absence of a db I chose to write data to JSON files which would have been neat but I forgot to give them proper unique indexes<br />
There is a lot missing which would need to be addressed like: all validation (within the setters) and a the album should get the artist name from the artist class but hopefully you see where I was going<br />

